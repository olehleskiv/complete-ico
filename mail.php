<?php
  // Pipedrive API token
  $api_token = 'd67568e8c1d18ec8cb91b6afb8d30437d595351f';
  
  //URL for Deal listing with your company domain name and $api_token variable
  $url = 'https://bluepes.pipedrive.com/v1/deals?api_token=' . $api_token;

  if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if ($_POST['type'] == 0 OR $_POST['type'] == 1 ) {
      $type = 'Standard ICO launch package';
    } else {
      $type = 'Additional services for your ICO launch';
    }

    $deal = array(
      'title' => 'Complete ICO message',
      '9ba919f984933f323d806ac1f2a30c064a9f9935' => $type,
      'af5b6a7b5fc7938f334204b6408dc852a755a2cd' => str_replace(array('\r','\n'), array(' ',' ') , strip_tags(trim($_POST['name']))),
      '7b16d8b439620b2ad7351bde7f5850350bf88d22' => filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL)
    );
     
    //POST request
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $deal);
    
    // Sender Data
    $name = str_replace(array("\r","\n"),array(" "," ") , strip_tags(trim($_POST["name"])));
    $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);

    if ( empty($name) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
      // Set a 400 (bad request) response code and exit.
      http_response_code(400);
      echo "Please, fill out the required (*) fields.";
      exit;
    }

    $output = curl_exec($ch);
    curl_close($ch);

    // Create an array from the data that is sent back from the API
    // As the original content from server is in JSON format, you need to convert it to PHP array
    $result = json_decode($output, true);
    if (!empty($result['data']['id'])) {
      http_response_code(200);
      echo "We got your email and will be in touch as soon as possible!";
    } else {
      http_response_code(500);
      echo "Please, fill out the required (*) fields.";
    }
} else {
  # Not a POST request, set a 403 (forbidden) response code.
  http_response_code(403);
  echo "There was a problem with your submission, please try again.";
}

?>
