#!/bin/bash

# Install dependencies only for Docker.
[[ ! -e /.dockerinit ]] && [[ ! -e  /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git -yqq
