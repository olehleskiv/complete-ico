(function ($) {
  'use strict';

  var form = $('#contact-form'),
      successMessage = $('.form-message-success'),
      errorMessage = $('.form-message-error'),
      formData,
      isValid = false;

  new WOW().init();

  // Success function
  function successCallback(response) {
    successMessage.fadeIn().removeClass('alert-danger').addClass('alert-success');
    successMessage.text(response);
    console.log(response);
    setTimeout(function () {
      successMessage.fadeOut();
    }, 5000);
    form.find('input:not([type="submit"])').val('');
  }


  // Error function
  function errorCallback(data) {
    console.log(data);
    errorMessage.fadeIn().removeClass('alert-success').addClass('alert-danger');
    setTimeout(function () {
      errorMessage.fadeOut();
    }, 5000);
  }
  
  form.submit(function (e) {
      e.preventDefault();
      formData = $(this).serialize();
      console.log(formData);

      $('input').each(function() {
        var input = $(this);
        inputValidate(input);
      });

      if (isValid) {
        $.ajax({ 
            type: 'POST',
            url: form.attr('action'),
            data: formData
        })
        .done(successCallback)
        .fail(errorCallback);
      }
  });

  $('.faq h5 button').click(function(event){

    setTimeout(function () {
      $('.faq h5 button[aria-expanded="false"] i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
    }, 0);

    if (event.target.children.length) {
      changeIcon(event.target.children);
    } else {
      changeIcon(event.target);
    }
  });

  function changeIcon(element) {
    if( $(element).hasClass('fa-chevron-up')){
      $(element).removeClass('fa-chevron-up').addClass('fa-chevron-down');
    } else {
      $(element).removeClass('fa-chevron-down').addClass('fa-chevron-up');
    }
  }

  $('#dropdownMenuButton').click(function(event){
    var expanded = $('#dropdownMenuButton').attr('aria-expanded');
    if (expanded == 'false') {
      $(event.target.children).removeClass('fa-chevron-down').addClass('fa-chevron-up');
    } else {
      $(event.target.children).removeClass('fa-chevron-up').addClass('fa-chevron-down');
    }
  });

  $("#quote-btn1").click( function(e) {
    $('.list .option')[1].click();
    e.preventDefault();
    $('body, html').animate({scrollTop: form.offset().top - 300}, 1000);
    $('#name').focus();
  });

  
  $("#quote-btn2").click( function(e) {
   $('.list .option')[2].click();
    e.preventDefault();
    $('body, html').animate({scrollTop: form.offset().top - 300 }, 1000);
    $('#name').focus();
  });

  // handle links with @href started with '#' only
  $(document).on('click', 'a[href^="#"]', function(e) {
    // target element id
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
      return;
    }
    e.preventDefault();

    // top position relative to the document
    var pos = $id.offset().top;

    // animated top scrolling
    $('body, html').animate({scrollTop: pos}, 1000);
  });

  $(document).ready(function() {
    $('select').niceSelect();
  });

  function inputValidate(input) {
    var required = input.prop('required');
    var empty = (input.val() == '' || input.val() == null);
    var type = input.prop('type');
    var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
	  var isEmail = re.test(input.val());

    if ((required && empty) || (type == 'email' && !isEmail)) {
      // disable submit button
      input.addClass('invalid');
      $('.form-message-error').fadeIn();
      $('#form-submit').prop('disabled', 'disabled');
      isValid = false;
    } else {
      input.removeClass('invalid');
      $('.form-message-error').fadeOut();
      $('#form-submit').removeAttr('disabled');
      isValid = true;
    }
  }
})(jQuery);